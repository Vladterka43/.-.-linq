﻿using System;
using ConsoleApplication13;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            bag = new MyBag();
        }

        private IMyBag bag;

        [TestMethod]
        public void Сердяння_цена_товара()
        {
            var rez = System.Math.Round(bag.Сердяння_цена_товара(), 2);
            Assert.AreEqual(rez, 103.93D);
        }

        [TestMethod]
        public void Количество_Товаров()
        {
            var rez = bag.Количество_Товаров();
            Assert.AreEqual(rez, 15);
        }

        [TestMethod]
        public void Сумарная_Цена_Товара()
        {
            var rez = System.Math.Round(bag.Сумарная_Цена_Товара(), 2);
            Assert.AreEqual(rez, 1559);
        }

        [TestMethod]
        public void Средня_Цена_Пива()
        {
            var rez = System.Math.Round(bag.Средня_Цена_Пива(), 2);
            Assert.AreEqual(rez, 132.5);
        }

        [TestMethod]
        public void Количество_Товаров_С_Ценой_Меньше()
        {
            var rez = bag.Количество_Товаров_С_Ценой_Меньше(43);
            Assert.AreEqual(rez, 6);
        }

        [TestMethod]
        public void Существует_Ли_Товар_с_Ценой()
        {
            var rez = bag.Существует_Ли_Товар_с_Ценой(412);
            Assert.IsTrue(rez);

            rez = bag.Существует_Ли_Товар_с_Ценой(9);
            Assert.IsFalse(rez);
        }

        [TestMethod]
        public void Количество_Товаров_ЗАканичив_НА()
        {
            var rez = bag.Количество_Товаров_ЗАканичив_НА("и");
            Assert.AreEqual(rez, 4);
        }

        [TestMethod]
        public void Середняя_Цена_Товара_По_ID()
        {
            var rez = System.Math.Round(bag.Середняя_Цена_Товара_По_ID(new []{1}), 2);
            Assert.AreEqual(rez, 10d);

            rez = System.Math.Round(bag.Середняя_Цена_Товара_По_ID(new[] { 17, 64, 82 }), 2);
            Assert.AreEqual(rez, 180.33);
        }

        [TestMethod]
        public void Список_Пролуктов_По_ID()
        {
            var rez = bag.Список_Пролуктов_По_ID(new[] { 17 });
            Assert.AreEqual(rez.Count, 1);
            Assert.AreEqual(rez[0], "Яблоки");

            rez = bag.Список_Пролуктов_По_ID(new[] { 64, 82 });
            Assert.AreEqual(rez.Count, 2);
            Assert.AreEqual(rez[1], "Сухофрукты");
        }

    }
}
