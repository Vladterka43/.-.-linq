﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication13
{
    public interface IMyBag
    {
        double Сердяння_цена_товара();
        int Количество_Товаров();
        double Сумарная_Цена_Товара();
        double Средня_Цена_Пива();
        int Количество_Товаров_С_Ценой_Меньше(int prise);
        bool Существует_Ли_Товар_с_Ценой(int prise);
        int Количество_Товаров_ЗАканичив_НА(string str);
        double Середняя_Цена_Товара_По_ID(int [] ids);
        List<string> Список_Пролуктов_По_ID(int[] ids);
    }

    public class MyBag : IMyBag
    {
        public MyBag()
        {
            Products = new List<Product>()
            {
                new Product() {Id = 1, Name = "Помидоры", Prise = 10},
                new Product() {Id = 2, Name = "Огурцы", Prise = 12},
                new Product() {Id = 17, Name = "Яблоки", Prise = 7},
                new Product() {Id = 4, Name = "Груши", Prise = 53},
                new Product() {Id = 5, Name = "Хлеб", Prise = 18},
                new Product() {Id = 64, Name = "Оливки", Prise = 122},
                new Product() {Id = 7, Name = "Колбаса", Prise = 255},
                new Product() {Id = 82, Name = "Сухофрукты", Prise = 412},
                new Product() {Id = 9, Name = "Рыба", Prise = 85},
                new Product() {Id = 10, Name = "Батон", Prise = 252},
                new Product() {Id = 111, Name = "Спички", Prise = 4},
                new Product() {Id = 12, Name = "Яйца", Prise = 6},
                new Product() {Id = 13, Name = "Пиво темно", Prise = 125},
                new Product() {Id = 49, Name = "Пиво светлое", Prise = 140},
                new Product() {Id = 15, Name = "Масло", Prise = 58},
            };    
        } 
        public List<Product> Products;

        public double Сердяння_цена_товара()
        {
            throw new NotImplementedException();
        }

        public int Количество_Товаров()
        {
            return Products.Count;
        }

        public double Сумарная_Цена_Товара()
        {
            throw new NotImplementedException();
        }


        public double Средня_Цена_Пива()
        {
            throw new NotImplementedException();
        }

        public int Количество_Товаров_С_Ценой_Меньше(int prise)
        {
            throw new NotImplementedException();
        }

        public bool Существует_Ли_Товар_с_Ценой(int prise)
        {
            throw new NotImplementedException();
        }

        public int Количество_Товаров_ЗАканичив_НА(string str)
        {
            throw new NotImplementedException();
        }

        public double Середняя_Цена_Товара_По_ID(int[] ids)
        {
            throw new NotImplementedException();
        }

        public List<string> Список_Пролуктов_По_ID(int[] ids)
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
